import { Router } from "express";
import { Person } from "../schema/model.js";
import successResponse from "../helper/successResponse.js";

let personRouter = Router();

personRouter
.route("/")
.post(async (req, res, next) => {

    let data= req.body;

    try {
        let result = await Person.create(data);
        successResponse(res, 201, "Person created successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})
.get(async (req, res, next) => {
    
    let id = req.params.id

    try {
        let result = await Person.find();
        successResponse(res, 200, "Person read successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})

personRouter
.route("/:id")
.patch(async (req, res, next) => {
    
    let id = req.params.id;
    let data = req.body;

    try {
        let result = await Person.findByIdAndUpdate(id, data);
        successResponse(res, 201, "Person updated successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})
.delete(async (req, res, next) => {
    
    let id = req.params.id;

    try {
        let result = await Person.findByIdAndDelete(id);
        successResponse(res, 200, "Person deleted successfully.");
    } catch (error) {
        console.log(error.message);
    }
})

export default personRouter