import { Router } from "express";
import { createRegister, deleteSpecificRegister, getMyProfile, loginRegister, readRegister, readSpecificRegister, updateMyProfile, updateSpecificRegister } from "../controllers/registerController.js";
import isAuthenticated from "../middleware/isAuthenticated.js";
import isAuthorized from "../middleware/isAuthorized.js";
 
let registerRouter = Router();

registerRouter
  .route("/").post(createRegister).get(readRegister);

  registerRouter.route("/login").post(loginRegister);

  registerRouter.route("/my-profile").get(isAuthenticated, getMyProfile);
  
  registerRouter
    .route("/my-profile-update")
    .patch(isAuthenticated, updateMyProfile);
    
  registerRouter
    .route("/:id")
    .delete(isAuthenticated, isAuthorized(["admin", "superAdmin"]), deleteSpecificRegister)

// registerRouter
//   .route("/:id")
//   .get(readSpecificRegister)
//   .delete(deleteSpecificRegister)
//   .patch(updateSpecificRegister);

export default registerRouter;