import { Router } from "express";
import upload from "../middleware/uploadFile.js";
import {
  createFile,
  createMultipleFiles,
} from "../controllers/fileController.js";

let fileRouter = Router();

fileRouter.route("/single").post(upload.single("file"), createFile);
fileRouter.route("/multiple").post(upload.array("files"), createMultipleFiles);

export default fileRouter;
