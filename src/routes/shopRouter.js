import { Router } from "express";
import { Shop } from "../schema/model.js";
import successResponse from "../helper/successResponse.js";

let shopRouter = Router();

shopRouter
.route("/")
.post(async (req, res, next) => {
    let data= req.body;

    try {
        let result = await Shop.create(data);
        successResponse(res, 201, "Shop created successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})
.get(async (req, res, next) => {
    let id = req.params.id

    try {
        let result = await Shop.find();
        successResponse(res, 200, "Shop read successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})

shopRouter
.route("/:id")
.patch(async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;

    try {
        let result = await Shop.findByIdAndUpdate(id, data);
        successResponse(res, 201, "Shop updated successfully.", result);
    } catch (error) {
        console.log(error.message);
    }
})
.delete(async (req, res, next) => {
    let id = req.params.id;

    try {
        let result = await Shop.findByIdAndDelete(id);
        successResponse(res, 200, "Shop deleted successfully.");
    } catch (error) {
        console.log(error.message);
    }
})

export default shopRouter