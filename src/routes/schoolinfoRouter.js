import { Router } from "express";
import { SchoolInfo } from "../schema/model.js";
import schoolinfoSchema from "../schema/schoolinfoSchema.js";
import successResponse from "../helper/successResponse.js";

let schoolInfoRouter = Router()

schoolInfoRouter
.route("/")
.post(async (req, res, next) => {
    let data = req.body;

    try {
      let result = await SchoolInfo.create(data);
      successResponse(res, 201, "School info created successfully", result);
    } catch (error) {
      console.log(error.message);
    }
})
.get(async (req, res, next) => {
    try {
      
        let result = await SchoolInfo.find({});
      successResponse(res, 201, "School info read successfully", result);
    } catch (error) {
      console.log(error.message);
    }
}
)

schoolInfoRouter
.route("/:id")
.patch(async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;

    try {
      let result = await SchoolInfo.findByIdAndUpdate(id, data);
      successResponse(res, 201, "Updated School Info detail successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  })
  .delete(async (req, res, next) => {
    let id = req.params.id;

    try {
      let result = await SchoolInfo.findByIdAndDelete(id);
      successResponse(res, 200, "Deleted School Info successfully", result);
    } catch (error) {
      console.log(error.message);
    }
  })

  export default schoolInfoRouter;