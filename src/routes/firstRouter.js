// define task for each request
//define Router
//use that router to the index.js (main file)

import { Router } from "express";

export let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000/products
  .post((req, res, next) => {
    console.log("product post");
    next("rrr");
  })
  .get((req, res, next) => {
    console.log("product get");
    next();
  })
  .patch(() => {
    console.log("product patch");
  })
  .delete(() => {
    console.log("product delete");
  });
