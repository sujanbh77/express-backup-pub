import { Router } from "express";
import { createGirlfriend, deleteSpecificGirlfriend, readGirlfriend, readSpecificGirlfriend, updateSpecificGirlfriend } from "../controllers/girlfriendController.js";

let girlfriendRouter = Router();

girlfriendRouter
.route("/")
.post(createGirlfriend)
.get(readGirlfriend)

girlfriendRouter
.route("/:id")
.get(readSpecificGirlfriend)
.patch(updateSpecificGirlfriend)
.delete(deleteSpecificGirlfriend)

export default girlfriendRouter;