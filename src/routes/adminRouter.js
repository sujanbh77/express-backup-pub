import { Router } from "express";
import { createAdmin, deleteSpecificAdmin, readAdmin, readSpecificAdmin, updateSpecificAdmin } from "../controllers/adminController.js";
let adminRouter = Router();

adminRouter
  .route("/") //localhost:8000/admins
  .post(createAdmin)
  .get(readAdmin)

adminRouter
  .route("/:id")
  .get(readSpecificAdmin)
  .delete(deleteSpecificAdmin)
  .patch(updateSpecificAdmin);

export default adminRouter;

//dynamic routes

// patch
//id => params
//data => body
