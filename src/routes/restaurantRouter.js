import { Router } from "express";
import { createRestaurant, deleteSpecificRestaurant, readRestaurant, readSpecificRestaurant, updateSpecificRestaurant } from "../controllers/RestaurantController.js";

let restaurantRouter = Router();

restaurantRouter.route("/").post(createRestaurant).get(readRestaurant);

restaurantRouter
  .route("/:id")
  .get(readSpecificRestaurant)
  .delete(deleteSpecificRestaurant)
  .patch(updateSpecificRestaurant);

export default restaurantRouter;