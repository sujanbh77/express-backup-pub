//crud
// Alt + Shift + o: Organized imports. (Removes unused imports.)



import successResponse from "../helper/successResponse.js";
import expressAsyncHandler from "express-async-handler";
import { Admin } from "../schema/model.js";

export let createAdmin = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;
  let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,15}$/.test(data.password);
    if (!isValid) {
      let error = new Error("Password must have: at least one lowercase and one uppercase letter, one special character, one digit, and 8-15 characters long and cannot have spaces.")
      throw error
    }
    
    data.password = await hashPassword(data.password);
  
  let result = await Admin.create(data);
  successResponse(res, 201, "Admin created successfully", result);
});

export let readAdmin = expressAsyncHandler(async (req, res, next) => {
    // console.log(req.query);
  let result = await Admin.find({name: req.query.name}).select(req.query.select).sort(req.query.sort);
  // let result = await Admin.find({ name: "bindu" });
  // let result = await Admin.find({ name: "bindu", age: 20 });
  // let result = await Admin.find({});
  // let result = await Admin.find({ "location.country": "finland" });

  // let result = await Admin.find({}).select("name email gender -_id");
  // let result = await Admin.find({ gender: "female" }).select(
  //   "name email gender -_id"
  // );

  //selecting field of document
  // let result = await Admin.find({}).select("name location.country -_id");

  //sorting
  // let result = await Admin.find({}).sort("name ");
  // let result = await Admin.find({}).sort("name -age");
  // let result = await Admin.find({}).sort("");

  //limit
  //skip
  // let result = await Admin.find({}).limit("3")//it means only show 3 object
  // let result = await Admin.find({}).skip("3"); //it means skip 3 object
  // let result = await Admin.find({}).skip("2").limit("10"); //it means skip 3 object

  //select
  //select has control over the field of object
  //  -_id means dont show id
  //sort
  //limit
  //skip

//   ---------------------------------------------------------------------------------------

// let result = await Admin.find({});
      // let result = await Admin.find({name: "bindu"});
      // let result = await Admin.find({name: "bindu", age: 20});
      // let result = await Admin.find({age: 20});             
      // let result = await Admin.find({isMarried: true});   // Find treats all data types as strings much like a google search.

      // let result = await Admin.find({age: {$gt: 18}});    // Greater than: {$gt: }     
      // let result = await Admin.find({age: {$gte: 18}});   // Greater than or equal to: {$gte: }     
      // let result = await Admin.find({age: {$lt: 18}});    // Less than: {$lt: }     
      // let result = await Admin.find({age: {$lte: 18}});   // Less than or equal to: {$lte: }     
      // let result = await Admin.find({age: {$ne: 18}});    // Not equal to: {$ne: }   
      
      // let result = await Admin.find({name: {$in: ["nitan", "hari"]}});    // Including: {$in: []}
      // let result = await Admin.find({name: {$in: ["nitan", "hari"]}, age: 21});

      // let result = await Admin.find({$or: [{name: "nitan"}, {name: "hari"}]});  //OR: {$or: [{}]}
      // let result = await Admin.find({$and: [{name: "nitan"}, {name: "hari"]}});  //AND: {$and: [{}]}

      // let result = await Admin.find({$and: [{age: {$gte: 20}, {age: {$lte: 24]}});  //AND: {$and: [{}]}
      
      // let result = await Admin.find({name: /nitan/});      // Regex Searching: Not exact searching. i.e. abcnitandef produces output.
      // let result = await Admin.find({name: "nitan"});      // Exact Searching.
      // let result = await Admin.find({name: /^nitan/});     // Starts with: /^../
      // let result = await Admin.find({name: /nitan$/});     // Starts with: /..$/

      // Select:
      // let result = await Admin.find({}).select("name email gender location.country -_id")    // Select: Selects specified attributes from the data returned by the find method.
      
      // Sort:      
      // let result = await Admin.find({}).sort("name age")              // Ascending Sort
      // let result = await Admin.find({}).sort("-name -age")            // Descending Sort
      // let result = await Admin.find({}).sort("")                      // No Sort
      // let result = await Admin.find({}).sort("name -age")             // Mixed Sort

      // Pagination:
      // let result = await Admin.find({}).limit("3")                    // Limits the number of results.
      // let result = await Admin.find({}).skip("2")                     // Skips the required number of results.
      // let result = await Admin.find({}).skip("2").limit(2)            // Skips the required number of results before limiting the remaining results.

      // Pagination flow: 1. Find
      //                  2. Sort
      //                  3. Select
      //                  4. Skip
      //                  4. Limit

      // Set:
      // Removes duplicate values from arrays, when converted into set.
      
      // let ar1 [ 1, 2, 1];
      // let uniqueValue = [...new Set(ar1)];
      // console.log(uniqueValue);

    //   ----------------------------------------------------------------------------------------------------
  successResponse(res, 200, "read admin successfully", result);
});

export let readSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Admin.findById(id);
  successResponse(res, 200, "read admin detail successfully", result);
});

export let updateSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;
  let result = await Admin.findByIdAndUpdate(id, data);
  successResponse(res, 201, "update admin detail successfully", result);
});

export let deleteSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Admin.findByIdAndDelete(id);
  successResponse(res, 200, "delete admin  successfully", result);
});
