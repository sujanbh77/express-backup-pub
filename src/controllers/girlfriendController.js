import successResponse from "../helper/successResponse.js";
import expressAsyncHandler from "express-async-handler";
import { Girlfriend } from "../schema/model.js";

export let createGirlfriend = expressAsyncHandler(async (req, res, next) => {
    let data = req.body;
    let result = await Girlfriend.create(data);
    successResponse(res, 201, "Girlfriend created successfully.", result);
})

export let readGirlfriend = expressAsyncHandler(async (req, res, next) => {
    let data = req.body;
    let result = await Girlfriend
    .find({properties: req.query.properties}
    .sort(req.query.sort)
    .select(req.query.select));
    successResponse(res, 200, "Girlfriend read successfully.", result);
})

export let readSpecificGirlfriend = expressAsyncHandler(async (req, res, next) => {
    let id = req.params.id;
    let result = await Girlfriend.findById(id)
    successResponse(res, 200, "Specific Girlfriend read successfully.", result);
})

export let updateSpecificGirlfriend = expressAsyncHandler(async (req, res, next) => {
    let data = req.body;
    let id = req.params.id;
    let result = await Girlfriend.findByIdAndUpdate(id, data);
    successResponse(res, 201, "Specific Girlfriend updated successfully.", result);
})

export let deleteSpecificGirlfriend = expressAsyncHandler(async (req, res, next) => {
    let id = req.params.id;
    let result = await Girlfriend.findByIdAndDelete(id);
    successResponse(res, 200, "Specific Girlfriend deleted successfully.", result);
})