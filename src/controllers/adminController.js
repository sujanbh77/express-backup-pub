import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Admin } from "../schema/model.js";

export let createAdmin = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;

  let result = await Admin.create(data);
  successResponse(res, 201, "Admin created successfully", result);
});

export let readAdmin = expressAsyncHandler(async (req, res, next) => {
  let result = await Admin.find({});
  successResponse(res, 200, "read admin successfully", result);
});

export let readSpecificAdmin = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Admin.findById(id);
  successResponse(res, 200, "read admin detail successfully", result);
});

export let updateSpecificAdmin = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Admin.findByIdAndUpdate(id, data);
    successResponse(res, 201, "update admin detail successfully", result);
  }
);

export let deleteSpecificAdmin = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Admin.findByIdAndDelete(id);
    successResponse(res, 200, "delete admin  successfully", result);
  }
);