import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Product } from "../schema/model.js";
import { sendMail } from "../utils/sendMail.js";

export let createProduct = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;

await sendMail({
  from: '"Sujan" <sujanbh77@gmail.com>',
  to: ["dila.bhusal7@gmail.com", "nitanthapa425@gmail.com"],
  subject: "My first system mail.",
  html: `<h1>This is the first system mail sent from the server.<h1>`,
});

  let result = await Product.create(data);
  successResponse(res, 201, "Product created successfully", result);
});

export let readProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.find({});
  successResponse(res, 200, "Read product successfully", result);
});

export let readSpecificProduct = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await Product.findById(id);
  successResponse(res, 200, "Read product detail successfully", result);
});

export let updateSpecificProduct = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Product.findByIdAndUpdate(id, data);
    successResponse(res, 201, "Updated product detail successfully", result);
  }
);

export let deleteSpecificProduct = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Product.findByIdAndDelete(id);
    successResponse(res, 200, "Deleted product  successfully", result);
  }
);