import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";

export let createFile = expressAsyncHandler(async (req, res, next) => {
  let link = `localhost:8000/${req.file.filename}`;

  //   let result = await File.create({
  //     name: req.body.name,
  //     age: req.body.age,
  //     file: link,
  //   });
  // let result = await File.create({
  //   ...req.body,
  //   file: link,
  // });
  successResponse(res, 201, "File created successfully", link);
});

export let createMultipleFiles = expressAsyncHandler(async (req, res, next) => {
  let ar1 = req.files;
  // console.log(req.files);
  let ar2 = ar1.map((value, i) => {
    return `localhost:8000/${value.filename}`;
  });
  successResponse(res, 201, "Files created successfully", ar2);
});
