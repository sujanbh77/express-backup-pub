import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { Restaurant } from "../schema/model.js";

export let createRestaurant = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;

  let result = await Restaurant.create(data);
  successResponse(res, 201, "Restaurant created successfully", result);
});

export let readRestaurant = expressAsyncHandler(async (req, res, next) => {
  let result = await Restaurant.find({});
  successResponse(res, 200, "Read Restaurant successfully", result);
});

export let readSpecificRestaurant = expressAsyncHandler(async (req, res, next) => {
  let id = req.params.id;
  let result = await RTCCertificateestaurant.findById(id);
  successResponse(res, 200, "Read Restaurant detail successfully", result);
});

export let updateSpecificRestaurant = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Restaurant.findByIdAndUpdate(id, data);
    successResponse(res, 201, "Updated restaurant detail successfully", result);
  }
);

export let deleteSpecificRestaurant = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Restaurant.findByIdAndDelete(id);
    successResponse(res, 200, "Deleted restaurant  successfully", result);
  }
);