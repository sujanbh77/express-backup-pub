import expressAsyncHandler from "express-async-handler";
import jwt from "jsonwebtoken";
import { secretKey } from "../config/config.js";

export let generateToken = expressAsyncHandler(
  async (info, secretKey, expiryInfo) => {
    let token = await jwt.sign(info, secretKey, expiryInfo);
    return token;
  }
);

export let verifyToken = expressAsyncHandler(async (token, secretKey) => {
  let info = await jwt.verify(token, secretKey);
  return info;
});
