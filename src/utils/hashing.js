import bcrypt from "bcrypt";

export let hashPassword = async (password) => {
    let _hashPassword = await bcrypt.hash(password, 10);
    return _hashPassword
}

// let data = await hashPassword("abc");

// console.log(data);

export let comparePassword = async (password, data) => {
    let isValidPassword = await bcrypt.compare(password, data)
    return isValidPassword
}

// let isValid = await comparePasswrod("abc");

// console.log(isValid);

// ---------------------------------------------------------------------------------------

// password    =