import expressAsyncHandler from "express-async-handler";
import { verifyToken } from "../utils/token.js";
import { secretKey } from "../config/config.js";

let isAuthenticated = expressAsyncHandler(async (req, res, next) => {
  let bearerToken = req.headers.authorization;
  // let token = req.headers.authorization.split(" ")[1]
  let arrBearerToken = bearerToken.split(" ");
  let token = arrBearerToken[1];

  let info = await verifyToken(token, secretKey);
  req.info = info;
  next();
});

export default isAuthenticated;
