import { Schema } from "mongoose";

let girlfriendSchema = Schema({
    name: {
        type: String,
        required: [true, "Name is required."],
        minlength: [4, "Name must contain at least 4 characters."],
        maxlength: [7, "Name must contain 7 characters at most."],
        validate: (value) => {
            let isValid = /^[A-Za-z ]+$/.test(value);
            if (!isValid) {
                let error = new Error("Name must contain only alphabets and spaces.")
                throw error
            }
        }
    },
    age: {
        type: Number,
        required: [true, "Age is required."],
        min: [20, "Age must be more than 20."],
        max: [25, "Age must be less than 25."],
    },
    properties: {
        intelligence: {
            type: String,
            required: [true, "Intelligence is required."],
        },
        senseOfHumor: {
            type: Boolean,
        }
    },
    physicalAppearance: {
        description: {
                type: String,
            },
        specificFeatures: {
                type: String,
            }      
    },
    relationshipExpectations: {
        commitment: {
                type: Boolean,
            },
        trustAndLoyalty: {
                type: Boolean,
            },
        compatibility: {
                type: Boolean,
            }
    },
    future_plans: {
        family_and_children: {
                type: Boolean,
            },
        career_and_personal_growth: {
                type: String,
            }
    }
})

export default girlfriendSchema;