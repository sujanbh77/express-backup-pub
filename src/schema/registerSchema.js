import { Schema } from "mongoose";

let registerSchema = Schema({
  fullName: {
    type: String,
    required: [true, "Name field is required"],
    minlength: [4, "Name field must at least 4 characters."],
    maxlength: [20, "Name field can only be 10 characters at most."],
    validate: (value) => {
      let isValid = /^[a-zA-Z]+$/.test(value);
      if (!isValid) {
        let error = new Error("Name field can only contain alphabets.");
        throw error;
      }
    },
  },
  email: {
    type: String,
    required: [true, "Email field is required."],
    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
        value
      );
      if (!isValid) {
        let error = new Error("Invalid email address.");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: [true, "Password is required."],
    // validate: (value) => {
    //   let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,15}$/.test(
    //     value
    //   );
    //   if (!isValid) {
    //     let error = new Error(
    //       "Password must have: at least one lowercase and one uppercase letter, one special character, one digit, cannot have spaces and must be 8-15 characters long."
    //     );
    //     throw error;
    //   }
    // },
  },
  role: {
    type: String,
    required: true,
  },
});

export default registerSchema;
