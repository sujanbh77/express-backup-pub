import { Schema } from "mongoose";

let restaurantSchema = Schema({
  name: {
    type: String,
    required: [true, "Name field is required"],
    minLength: [4, "Name field must be at least 4 character"],
    maxLength: [10, "Name field must be at most 10 character"],
  },
  cuisineType: {
    type: Number,
  },

  isOpen: {
    type: Boolean,
  },
  openDate: { type: Date, default: new Date() },
});

export default restaurantSchema;