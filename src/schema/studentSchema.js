import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    type: String,
  },
  age: {
    type: Number,
  },
  file: {
    type: String,
  },
});

export default studentSchema;

// >> pass image from api.
// >> upload image to server.
// >> save the image link to database
