import { Schema } from "mongoose";

let shopSchema = Schema({
    name: {
        type: String,
        required: [true, "Name must consist of only alphabets."],
        minlength: [3, "Name must be more than 3 characters long."],
        maxlength: [30, "Name must be less than 30 characters long."],
    },
    address: {
        type: String,
        required: [true, "Address must consist of only alphabets."],
    },
    contact: {
        type: Number,
        validate: (value) => {
            let numberString = String(value)
            if(numberString.length != 10) {
                let error = new Error("Phone number be 10 digits.")
                throw error;
            }
        }
    },
    email: {
        type: String,
        required: [true, "Email is required."],
        validate: (value) => {
            let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
            if (!isValid) {
                let error = new Error("Invalid email.")
                throw error;
            }
        }
    },
    panNumber: {
        type: Number,
        required: [true, "PAN number is required."],
        validate: (value) => {
            let numberString = String(value)
            if (numberString.length != 12) {
                let error = new Error("PAN number must be 12 digits.")
                throw error;
            }
        }
    },
    proprietorName: {
        type: String,
        required: [true, "Name must consist of only alphabets."],
    },
})

export default shopSchema