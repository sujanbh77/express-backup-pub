import { Schema } from "mongoose";

let personSchema = Schema({
    name : {
        type : String,
        required: [true, "Name is required."],
        minlength: [3, "Name must be at least 3 characters long."],
        maxlength: [20, "Name must be less than 20 characters."],
        validate: (value) => {
            let isValid = /^[A-Za-z ]+$/.test(value);
            if (!valid) {
                let error = new Error("Name can only consist of letters and spaces.");
                throw error;
            }
        }
        
    },
    address : {
        type : String,

    },
    dob : {
        type : Date
    },
    gender : {
        type : String
    },
    isMarried :{
        type : Boolean
    },
    religious : {
        type : String
    },
    nationality : {
        type : String
    },
    education : {
        type : String
    },
    phoneNumber : {
        type : Number
    },
    email : {
        type : String
    }
})
export default personSchema;