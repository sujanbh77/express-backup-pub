// {
//   name: "",
//   age: 2,
//   password:"password@123",
//   roll: 2,
//   isMarried: true,
//   spouseName: "lkjl",
//   email: "abc@gmail.com",
//   gender: "male",
//   dob: 2019 - 2 - 2,
//   location: { country: "", exactLocation: "" },
//   favTeacher: ["a", "b", "c"],

//   favSubject: [
//     { bookName: "a", bookAuthor: "a" },
//     { bookName: "b", bookAuthor: "b" },
//   ],
// };

// Schema:

// Manipulation of data:

// lowercase: true
// uppercase: true
// trim: true
// unique: true
// default: "value"

// Validation of data:

// required: [true, "Name field is required"],

// String validation:

// minlength: [4, "Name field must at least 4 characters."],
// maxlength: [10, "Name field can only be 10 characters at most."],

// Number validation:

// min:[18, "Age must be at least 18"],
// max:[30, "Age must be at most 30"],

// Custom validation:

// validate: ()=>{}  //Custom function created to meet custom requirements.

import { Schema } from "mongoose";

let adminSchema = Schema({
  name: {
    type: String,
    // lowercase: true
    // uppercase: true,
    // trim: true,
    required: [true, "Name field is required"],
    minlength: [4, "Name field must at least 4 characters."],
    maxlength: [10, "Name field can only be 10 characters at most."],
    validate: (value) => {
      let isValid = /^[a-zA-Z]+$/.test(value);
      if (!isValid) {
        let error = new Error("Name field can only contain alphabets.");
        throw error;
      }
    }
  },
  
  // phoneNumber: {
  //   type: String,
  //   required: [true, "Phone number field is required"],
  //   minlength:[10, "Phone number must be at least 18"],
  //   maxlength:[10, "Phone number must be at most 30"], 
  // },
  
  // phoneNumber: {
  //   type: Number,
  //   required: [true, "Phone number field is required"],
  //   min:[1000000000, "Phone number must be at least 10 digits."],
  //   max:[9999999999, "Phone number must be at most 10 digits."], 
  // },

  phoneNumber: {
    type: Number,
    required: [true, "Phone number field is required"],
    validate: (value) => {
      let pString = String(value);
      if (pString.length != 10) {
        let error = new Error("Phone number must be 10 digits");
        throw error;
      }
    }
  },
  password: {
    type: String,
    required: [true, "Password is required"],
    validate: (value) => {
      let isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).{8,15}$/.test(value);
      if (!isValid) {
        let error = new Error("Password must have: at least one lowercase and one uppercase letter, one special character, one digit, and 8-15 characters long and cannot have spaces.")
        throw error
      }
    }
  },
  age: {
    type: Number,
    min:[18, "Age must be at least 18"],
    max:[30, "Age must be at most 30"], 
  },
  roll: {
    type: Number,
  },
  isMarried: {
    type: Boolean,
    required: [true, "Marital state field is required"],
  },
  spouseName: {
    type: String,
    required: [function () {
      if(this.isMarried === true) {
        return true;
      } else {
        this.spouseName = ""
        return false;
      }
    }, "Spouse name is required"
  ]
  },
  email: {
    type: String,
    // unique: true,
    required: [true, "Email field is required"],
    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
      if (!isValid) {
        let error = new Error("Invalid email address.")
        throw error
      }
    }
  },
  gender: { 
    type: String,
    default: "male",
    // required: [true, "Gender field is required"],
    // Required value is meaningless if default value is present.

    // validate: (value) => {
    //   if (value === "male" || value === "female" || value === "other") {

    //   } else {
    //     let error = new Error("Gender must be either male, female, or other.");
    //   }
    // }
    enum: {
    values: ["male", "female", "other"],
    message: (notEnum) => {
      return `${notEnum.value} is not valid enum`;
    }
  },
  },
  dob: { 
    type: Date 
  },
  location: { 
    country: { type: String }, 
    exactLocation: { type: String } 
  },
  favTeacher: [
    {
      type: String,
    },
  ],

  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});

export default adminSchema;
