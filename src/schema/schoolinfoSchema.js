import { Schema } from "mongoose";

let schoolinfoSchema = Schema({
name: { type: String, 
      required: [true, "Name must only consist of alphabets"], 
      minlength: [3, "Name must be longer than 3 characters"],
      maxlength: [35, "Name must not be longer than 35 characters"],},
address: { type: String, 
         required: [true, "Address must only consist of alphabets"]},
contact: { type: Number, 
         type: Number,
         validate: (data) => {
         let numberString = String(data);
         if (numberString.length != 10) {
         let error = new Error("Phone number must be of 10 characters");
         throw error;
        }},},
email: { type: String }});

export default schoolinfoSchema