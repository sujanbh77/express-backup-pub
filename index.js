//npm i express
// make express application =>
//attached port =>

import express, { Router, json } from "express";
import { firstRouter } from "./src/routes/firstRouter.js";
import mongoose, { Schema, model } from "mongoose";
import connectMongodb from "./src/connectdb/connectMongodb.js";
import userRouter from "./src/routes/userRouter.js";
import schoolRouter from "./src/routes/schoolRouter.js";
import adminRouter from "./src/routes/adminRouter.js";
import schoolInfoRouter from "./src/routes/schoolinfoRouter.js";
import shopRouter from "./src/routes/shopRouter.js";
import girlfriendRouter from "./src/routes/girlfriendRouter.js";
import productRouter from "./src/routes/productRouter.js";
import restaurantRouter from "./src/routes/restaurantRouter.js";
import reviewRouter from "./src/routes/reviewRouter.js";
// import personRouter from "./src/routes/personRouter.js";
import jwt from "jsonwebtoken";
import studentRouter from "./src/routes/studentRouter.js";
import errorResponse from "./src/helper/errorResponse.js";
import fileRouter from "./src/routes/fileRouter.js";
import { port } from "./src/config/config.js";
import cors from "cors"
import registerRouter from "./src/routes/registerRouter.js";

// localhost:8000/products  method post

let app = express();
app.use(json());
app.use(cors())

// app.use("/products", firstRouter);
app.use("/users", userRouter);
app.use("/schools", schoolRouter);
app.use("/admins", adminRouter);
app.use("/schoolInfo", schoolInfoRouter);
app.use("/shops", shopRouter);
// app.use("/persons", personRouter);
app.use("/girlfriends", girlfriendRouter);
app.use("/products", productRouter);
app.use("/restaurants", restaurantRouter);
app.use("/reviews", reviewRouter);
app.use("/students", studentRouter);
app.use("/files", fileRouter);
app.use("/registers", registerRouter);

connectMongodb();

// let info = {
//   name: "nitan",
//   surName: "thapa",
//   fullName: function () {
//     console.log(`${this.name} ${this.surName}`);
//   },
// }

// ...................................................

// Interview Question:

// Difference between normal and arrow functions:

// Syntax.

// Normal(old) function supports 'this' method.
// Arrow(new) function does not support 'this' method.

// ............................................................

// info.fullName();

// --------------------------------------------------------------------------------------------------------

// bcrypt
// npm i bcrypt

// bcrypt.hash(password, 10);
// bcrypt.compare(password, hashPassword);

// -----------------------------------------------------------------------------------------------------

// npm i jsonwebtoken
// import jwt from "jsonwebtoken"

// ...........................................

// Token generation:

// let info = {
//   id: "12341234",
//   role: "admin"
// }

// let secretKey = "binsebi";

// let expiryInfo = {
//   expiresIn: "365d",  // Generally 1 year. [s: seconds, m: minute, h: hour, d: day, y:year]. Better practice to use d format.
// }

// let token = await jwt.sign(info, secretKey, expiryInfo);

// console.log(token)

// ............................................................

// Token Verification:

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQxMjM0Iiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNjg2ODg3ODA3LCJleHAiOjE3MTg0MjM4MDd9.KE_T6P0vlexHJuXgEuq_pq0XQH3teJjFhc1QPQsgYIM"
// let secretKey = "binsebi"
// let infoObj = await jwt.verify(token, secretKey);

// console.log(infoObj)

// ------------------------------------------------------------------------------------------------------------------------

app.use(express.static("./public")); //(./) is Root folder.

// Syntax for link: [localhost:8000/filename.extension]
// When using browser link, it directly searches in the static folders.

// Examples:
// localhost:8000/earth.jpg
// localhost:8000/canvas.png
// localhost:8000/mastermind.pdf

// multer: npm i multer, Used to import files and images to the server.

// ----------------------------------------------------------------------------------------------------------------

app.use(errorResponse);

app.listen(port, () => {
  console.log(`express application is listening at port ${port}`);
});
